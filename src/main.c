/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <sys/statvfs.h>
#include <simpleprint/simpleprint.h>

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
#define MX(a,b) (a > b ? a : b)

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void DumpStructure (struct statvfs *buf)
{
	PrintValue ("f_bsize   ", (int)(buf->f_bsize));
	PrintValue ("f_frsize  ", (int)(buf->f_frsize));
	PrintValue ("f_blocks  ", (int)(buf->f_blocks));
	PrintValue ("f_bfree   ", (int)(buf->f_bfree));
	PrintValue ("f_bavail  ", (int)(buf->f_bavail));
	PrintValue ("f_files   ", (int)(buf->f_files));
	PrintValue ("f_ffree   ", (int)(buf->f_ffree));
	PrintValue ("f_favail  ", (int)(buf->f_favail));
	PrintValue ("f_fsid    ", (int)(buf->f_fsid));
	PrintValue ("f_flag    ", (int)(buf->f_flag));
	PrintValue ("f_namemax ", (int)(buf->f_namemax));
}

int ValueToString (uint64_t value, char *out, int percent)
{
	if (percent)
	{
		_itoa (value, out);
		strcat (out, "%");
		return strlen (out);
	}

	if (value < 1024)
	{
		// Less than 1 KiB
		_itoa (value, out);
		strcat (out, "B");
	}
	else if (value < (1024 * 1024 * 8))
	{
		// Less than 8 MiB
		value /= 1024;
		_itoa (value, out);
		strcat (out, "KiB");
	}
	else if (value < (1024lu * 1024lu * 1024lu * 8lu))
	{
		// Less than 8 GiB
		value /= (1024 * 1024);
		_itoa (value, out);
		strcat (out, "MiB");
	}
	else
	{
		value /= (1024 * 1024 * 1024);
		_itoa (value, out);
		strcat (out, "GiB");
	}

	return strlen (out);
}

void AlignWithString (char *str, int len, int maxlen)
{
	SimplePrint (str);

	for (int i = 0; i < maxlen - len; i++)
		SimplePrint (" ");
}

void PrintDetails (char *name, struct statvfs *buf)
{
	uint64_t tot = (uint64_t)(buf->f_blocks) * (uint64_t)(buf->f_bsize);
	uint64_t fre = (uint64_t)(buf->f_bfree) * (uint64_t)(buf->f_bsize);

	char str_fs[] = "fatfs?";
	char str_size[32];
	char str_used[32];
	char str_avail[32];
	char str_percent[32];

	int len_fs = strlen (str_fs);
	int len_size = ValueToString (tot, str_size, 0);
	int len_used = ValueToString (tot - fre, str_used, 0);
	int len_avail = ValueToString (fre, str_avail, 0);
	int len_percent = ValueToString (((tot - fre) * 100) / tot, str_percent, 1);
	int len_mountedon = strlen (name);

	char a[] = "Filesystem";
	char b[] = "Size";
	char c[] = "Used";
	char d[] = "Avail";
	char e[] = "Use%";
	char f[] = "Mounted on";

	int lablen_fs = strlen (a);
	int lablen_size = strlen (b);
	int lablen_used = strlen (c);
	int lablen_avail = strlen (d);
	int lablen_percent = strlen (e);
	int lablen_mountedon = strlen (f);

	int maxlen_fs = MX (lablen_fs, len_fs) + 1;
	int maxlen_size = MX (lablen_size, len_size) + 1;
	int maxlen_used = MX (lablen_used, len_used) + 1;
	int maxlen_avail = MX (lablen_avail, len_avail) + 1;
	int maxlen_percent = MX (lablen_percent, len_percent) + 1;
	int maxlen_mountedon = MX (lablen_mountedon, len_mountedon) + 1;

	AlignWithString (a, lablen_fs, maxlen_fs);
	AlignWithString (b, lablen_size, maxlen_size);
	AlignWithString (c, lablen_used, maxlen_used);
	AlignWithString (d, lablen_avail, maxlen_avail);
	AlignWithString (e, lablen_percent, maxlen_percent);
	AlignWithString (f, lablen_mountedon, maxlen_mountedon);
	SimplePrint ("\n");

	AlignWithString (str_fs, len_fs, maxlen_fs);
	AlignWithString (str_size, len_size, maxlen_size);
	AlignWithString (str_used, len_used, maxlen_used);
	AlignWithString (str_avail, len_avail, maxlen_avail);
	AlignWithString (str_percent, len_percent, maxlen_percent);
	AlignWithString (name, len_mountedon, maxlen_mountedon);
	SimplePrint ("\n");
}

int main (int argc, char **argv)
{
	struct statvfs buf;

	if (argc < 2)
		SimplePrint ("Usage: df <drive>\n");
	else if (argc == 2 && !statvfs (argv[1], &buf))
		PrintDetails (argv[1], &buf);
	else if (argc == 3 && !statvfs (argv[2], &buf))
		DumpStructure (&buf);

	return 0;
}
